<?php

namespace DesignPatternObserver\App\interfaces;

interface Observer
{
    /**
     * @param Observable $observable
     */
    public function update(Observable $observable): void;
}