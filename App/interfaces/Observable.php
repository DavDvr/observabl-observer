<?php

namespace DesignPatternObserver\App\interfaces;

/**
 * As SplSubject
 * Interface Observable
 * @package DesignPatternObserver\App\interfaces
 */
interface Observable
{
    /**
     * Add or attach an Observer
     * @param Observer $observer
     */
    public function subscribe(Observer $observer):void;

    /**
     * Remove or detach an Observer
     * @param Observer $observer
     */
    public function unsubscribe(Observer $observer): void;

    /**
     * Notify all observers about an event
     */
    public function notifyObservers(): void;

}