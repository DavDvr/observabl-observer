<?php

use DesignPatternObserver\App\Class\ObservableImpl;
use DesignPatternObserver\App\Class\ObserverImpl1;
use DesignPatternObserver\App\Class\ObserverImpl2;

require_once (dirname(__DIR__) ."/vendor/autoload.php");


//create Observable instance
$observable = new ObservableImpl();

// create Observer instance
$observer1 = new ObserverImpl1();
$observer2 = new ObserverImpl2();
$observer3 = new ObserverImpl1();

//i must subscribe an Observer $observableImpl
$observable->subscribe($observer1);
$observable->subscribe($observer2);
$observable->subscribe($observer3);


//i change the state to observable
$observable->setState(441);
$observable->setState(43);

//i remove observer1
$observable->unsubscribe($observer1);

$observable->setState(88);