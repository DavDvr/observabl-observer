<?php

namespace DesignPatternObserver\App\Class;

use DesignPatternObserver\App\interfaces\Observable;
use DesignPatternObserver\App\interfaces\Observer;

class ObservableImpl implements Observable
{

    /**
     * Observers listen this variable "$state"
     * @var int
     */
    private int $state;

    /**
     * @var array<Observer>
     */
    private array $observers = [];

    public function __construct(int $state = 10, array $observers = [])
    {
        $this->state = $state;
        $this->observers = $observers;
    }
    /**
     * Add Observer
     * @inheritDoc
     */
    public function subscribe(Observer $observer): void
    {
        $this->observers[]= $observer;
    }

    /**
     * If the value exist in the array, i search $observer in array $observers and i remove it
     * @inheritDoc
     */
    public function unsubscribe(Observer $observer): void
    {
        if (in_array($observer, $this->observers)){
            unset($this->observers[array_search($observer, $this->observers)]);
        }
    }

    /**
     * Notify Observers
     * @inheritDoc
     */
    public function notifyObservers(): void
    {
        foreach ($this->observers as $ob){
            $ob->update($this);
        }
    }

    /**
     * Return the state
     * @return int
     */
    public function getState(): int
    {
        return $this->state;
    }

    /**
     * Modify the state
     * @param int $state
     */
    public function setState(int $state): void
    {
        $this->state = $state;
        $this->notifyObservers();
    }


}