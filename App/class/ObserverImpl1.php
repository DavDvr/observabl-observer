<?php

namespace DesignPatternObserver\App\Class;

use DesignPatternObserver\App\interfaces\Observer;
use DesignPatternObserver\App\interfaces\Observable;

class ObserverImpl1 implements Observer
{

    /**
     * Observer receive a notify to change then it take the state and do state x state + 9
     * @inheritDoc
     */
    public function update(Observable $observable): void
    {
        $state = $observable->getState();
        $result = $state * $state + 9;
        echo "______________ObserverImpl1____________________<br>";
        echo "<br>";
        echo "Nouvelle mise à jour: state = " . $state. " <br>";
        echo "Resultat de calcul: " .$result. " <br>";
        echo "<br>";
    }
}