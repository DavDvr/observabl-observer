<?php

namespace DesignPatternObserver\App\Class;

use DesignPatternObserver\App\interfaces\Observable;
use DesignPatternObserver\App\interfaces\Observer;

class ObserverImpl2 implements Observer
{

    /**
     * @var int
     */
    private int $counter;

    /**
     * Observer receive a notify to change then it take the state and do state x state + 9
     * @inheritDoc
     */
    public function update(Observable $observable): void
    {

        echo "________________ObserverImpl2_________________ <br>";
        echo "<br>";
        $counter = 10;
        $state = $observable->getState();
        ($state % 2 == 0 ? ++$counter : --$counter);

        echo "Nouvelle mise à jour: state= ". $state. " <br>";
        echo ($state % 2 === 0 ? "Le $state  est pair! <br>" : "Le $state est impair! <br>");
        echo "Le compteur est initalement a 10 il sera incrémenté de 1 si le nbr $state qui est retourné par l'observable est pair <br>
                Si le nbr $state est impair alors je décrémente la valeur de 1<br>
                Resultat : ". $counter. " <br>";
        echo "<br>";
    }
}